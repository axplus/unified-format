#!/usr/bin/env python
# 2014-8-9


from setuptools import setup


setup(name='format',
      version='0.2.1',
      author='Yijie Zeng',
      packages=['format'],
      data_files=[('etc', ['etc/uncrustify.conf'])],
      scripts=['fmt.py'],
      zip_safe=True)

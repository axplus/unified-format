# 2016-7-3


import codetypes


class Codefile(object):
    def __init__(self, path, codetype):
        self.path = path
        self.codetype = codetype


def understand(path):
    codetype = codetypes.guess_type(path)
    return Codefile(path, codetype)

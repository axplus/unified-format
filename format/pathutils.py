# 2014-7-2


import os


def enumeratepaths(thepaths):
    for mypath in thepaths:

        if os.path.isfile(mypath):
            yield mypath
            continue

        if not os.path.isdir(mypath):
            raise IOError()

        # recursively
        for r, ds, filenames in os.walk(mypath):
            for filename in filenames:
                filepath = os.path.join(r, filename)
                yield filepath

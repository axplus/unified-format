# 2016-7-2


import subprocess

STYLE = {
    'BasedOnStyle': 'google',
    'IndentWidth': 4
}

style_str = ','.join(['%s: %s' % (k, v) for k, v in STYLE.items()])


def beautify(codefile):
    subprocess.call(
        'clang-format -i -style="{%s}" %s' % (style_str, codefile._path),
        shell=True)

# 2014-6-12


import os
import subprocess
from sys import stderr

from . import codetypes

basedir = os.path.abspath(os.path.dirname(__file__))
CFG_PATH = os.path.join(basedir, 'etc',
                        'uncrustify.conf')  # os.path.expanduser('~/Dropbox/conf/uncrustify/default.conf')


def format(thepath):
    try:
        mytype = codetypes.guess_type(thepath)
        # call cli to perform format
        myproc = subprocess.Popen('uncrustify -c {cfg} -l {lang} --replace --no-backup {file}'.format(
            cfg=CFG_PATH, lang=mytype.desc, file=thepath), shell=True)
        myout, myerr = myproc.communicate()
        myproc.wait()
    except codetypes.UnknownTypeError:
        print >> stderr, 'cannot detect type of', thepath
        pass


def beautify(cf):
    subprocess.call(
        'uncrustify -c {cfg} -l {lang} --replace --no-backup {file}' % dict(
            cfg=CFG_PATH,
            lang=cf._codetype.desc,
            file=cf._path),
        shell=True)

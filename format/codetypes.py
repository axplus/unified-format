# 2014-6-12


__all__ = ['CodeType', 'UnknownTypeError', 'guess_type']

import logging
import os
import subprocess
from abc import ABCMeta
from inspect import isclass


class CodeType(object):
    __metaclass__ = ABCMeta

    @classmethod
    def ismytype(cls, path):
        content = file(path, 'rb').read()
        return cls.ismytype_fromcontent(content)


class UnknownTypeError(BaseException):
    pass


class C(CodeType):
    exts = ('.c',)  # '.h',)
    desc = 'C'

    @staticmethod
    def ismytype_fromcontent(thecontent):
        # handle all other types
        return False


class OC(CodeType):
    exts = ('.m',)
    desc = 'OC'

    @staticmethod
    def ismytype_fromcontent(thecontent):
        return '@interface' in thecontent or '@protocol' in thecontent


class OCpp(CodeType):
    exts = ('.mm',)
    desc = 'OC+'

    @staticmethod
    def ismytype_fromcontent(thecontent):
        return 'class' in thecontent and \
               '@interface' in thecontent


class Cpp(CodeType):
    exts = ('.hpp', '.cc', '.cpp', '.cxx', '.c++', '.hh')
    desc = 'CPP'

    @staticmethod
    def ismytype_fromcontent(thecontent):
        return 'class' in thecontent and \
               '@interface' not in thecontent


class _TypeManager(object):
    REGISTERED_TYPES = []  # (OCpp, Cpp, OC, C, )
    EXT_TO_TYPE = {}

    @staticmethod
    def guess_type(thepath):
        # ignore binary
        if _Utils.isbinary(thepath):
            logging.info('%s is binary file' % thepath)
            raise UnknownTypeError()

        myp, myext = os.path.splitext(thepath.lower())
        try:
            codetype = _TypeManager.EXT_TO_TYPE[myext]
            return codetype
        except KeyError:
            for ct in _TypeManager.REGISTERED_TYPES:
                if ct.ismytype(thepath):
                    return ct

        raise UnknownTypeError()


class _Utils(object):
    @staticmethod
    def isbinary(thepath):
        myout = subprocess.check_output(
            'file -I "{filepath}"'.format(filepath=thepath), shell=True)
        return '=binary' in myout


guess_type = _TypeManager.guess_type

# generate index
for k, v in globals().items():
    if isclass(v) and issubclass(v, CodeType) and v is not CodeType:
        _TypeManager.REGISTERED_TYPES.append(v)
        for ext in v.exts:
            _TypeManager.EXT_TO_TYPE[ext] = v

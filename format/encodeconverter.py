# 2014-6-12


import logging
from sys import stderr

import chardet


class _BuiltInConverter(object):
    @staticmethod
    def convert(thefrom, theto, thepath):
        myraw = open(thepath, 'rb').read()
        myunicode = myraw.decode(thefrom)
        mybuffer = myunicode.encode(theto)
        open(thepath, 'wb').write(mybuffer)


class _IconvConverter(object):
    @staticmethod
    def convert(thefrom, theto, thepath):
        myraw = open(thepath, 'rb').read()
        myout = subprocess.check_output(
            'iconv -f {frome} -t {toe}'.format(frome=thefrom, toe=theto), shell=True)
        # myout, _ = myproc.communicate(myraw)
        open(thepath, 'wb').write(myout)


class EncodeDecodeError(BaseException):
    pass


def format(thepath):
    """convert all encoding to utf-8"""
    myraw = open(thepath, 'rb').read()
    myencodingresult = chardet.detect(myraw)

    # support only gb2312/gbk to utf-8
    try:
        myencoding = myencodingresult['encoding'].lower()
    except AttributeError:
        logging.error('can not detect encode of "%s"' % thepath)
        # print >>stderr, '[] can detect encode', thepath
        raise EncodeDecodeError()

    if myencoding in ('utf-8', 'ascii'):  # == 'utf-8':
        # no need to convert
        return

    if myencoding not in ('gbk', 'gb2312', 'gb18030'):
        print >> stderr, '[] cannot decode', thepath, 'with', myencoding
        raise EncodeDecodeError()

    print >> stderr, '[] will convert', thepath
    myconverters = (_BuiltInConverter, _IconvConverter,)
    for myconverter in myconverters:
        try:
            _BuiltInConverter.convert(myencoding, 'utf-8', thepath)
            break
        except:
            raise EncodeDecodeError()


def beautify(codefile):
    return format(codefile._path)

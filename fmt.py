# 2014-4-8


import argparse

# format action
from format import clangformat, encodeconverter
from format.pathutils import enumeratepaths
from format import codetypes
from format import codefile
import logging


def main():
    # fmt.py [options] file ...
    logging.basicConfig(level=logging.INFO)

    myparser = argparse.ArgumentParser()
    myparser.add_argument('-n', '--dry-run', action='store_true')
    myparser.add_argument('files_or_dirs', nargs='+')  # argparse.REMAINDER)
    myargs = myparser.parse_args()

    myformatpipeline = (encodeconverter, clangformat,)
    for mypath in enumeratepaths(myargs.files_or_dirs):
        if myargs.dry_run:
            continue

        logging.info('-----------------%s' % mypath)
        try:
            cf = codefile.understand(mypath)  # codetypes.guess_type(mypath)
            logging.info('---------codetype===========%s' % cf._codetype)
            for myformatter in myformatpipeline:
                logging.info('=======formatter=%s' % myformatter)
                myformatter.beautify(cf)

        except codetypes.UnknownTypeError:
            logging.warning('can not guess type')
            continue


if __name__ == '__main__':
    main()
